// import React, { useState } from "react";
// import Modal from "./Modal";

// function Button(props){
//     const {info} = props
//     const [activeModal, setActiveModal] = useState(false)
//     const btn = <button onClick={()=>setActiveModal(true)} style={{background:info.backgroundColor}}>{info.text}</button>
//     return(
//         <div>
//             <div className="btn">
//              {btn}
//             </div>
//           <Modal info={info} active={activeModal} setActive={setActiveModal}/>
//         </div>
//     )
// }
// export default Button

import React, { Component } from 'react';
import Modal from './Modal';

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeModal: false
    };
  }

  setActiveModal = (value) => {
    this.setState({ activeModal: value });
  };

  render() {
    const { info } = this.props;
    const btn = (
      <button onClick={() => this.setActiveModal(true)} style={{ background: info.backgroundColor }}>
        {info.text}
      </button>
    );

    return (
      <div>
        <div className="btn">{btn}</div>
        <Modal info={info} active={this.state.activeModal} setActive={this.setActiveModal} />
      </div>
    );
  }
}

export default Button;