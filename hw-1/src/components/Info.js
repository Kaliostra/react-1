const modalInfo = [
    {
        backgroundColor : "yellow",
        text: "First Button",
        onClick: "",
        modal:{
            color:"yellow",
            id:"modal-1",
            title:"title for modal 1",
            description:"description for modal 1",
            className:"modalFirstBtn"
        }
    },
    {
        backgroundColor : "blue",
        text: "Second Button",
        onClick: "",
        modal:{
            color:"blue",
            id:"modal-2",
            title:"title for modal 2",
            description:"description for modal 2",
            className:"modalSecondBtn"
        }  
    }
]

export default modalInfo