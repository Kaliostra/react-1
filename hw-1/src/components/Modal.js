// import React from "react";

// const Modal =({info,active,setActive})=>{
//     return(
//         <div className={active ? "modal active" : "modal"} onClick={()=>setActive(false)}>
//             <div className="modal__info" style={{backgroundColor:info.modal.color}} onClick={e => e.stopPropagation()}>
//             <h2>{info.modal.id}</h2>
//             <p>{info.modal.description}</p>
//             <button className={info.modal.className}>Accept</button>
//             <button className={info.modal.className} onClick={()=> setActive(false)}>Close</button>
//             </div>
//         </div>
//     )
// }
// export default Modal

import React from "react";

class Modal extends React.Component {
  render() {
    const { info, active, setActive } = this.props;

    return (
      <div
        className={active ? "modal active" : "modal"}
        onClick={() => setActive(false)}
      >
        <div
          className="modal__info"
          style={{ backgroundColor: info.modal.color }}
          onClick={(e) => e.stopPropagation()}
        >
          <h2>{info.modal.id}</h2>
          <p>{info.modal.description}</p>
          <button className={info.modal.className}>Accept</button>
          <button
            className={info.modal.className}
            onClick={() => setActive(false)}
          >
            Close
          </button>
        </div>
      </div>
    );
  }
}

export default Modal;
